<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa;

use Europa\Exceptions\EuropaSDKException;

/**
 * Class EuropaCustomer.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaCustomer
{
    /**
     * @var string The Europa customer ID.
     */
    private $id;

    /**
     * Instantiates a new EuropaCustomer entity.
     *
     * @param string $id
     *
     * @throws EuropaSDKException
     */
    public function __construct($id)
    {
        // Require customer ID to be set as a string to prevent
        // possible PHP_INT_MAX overflow on some platforms.
        if (!is_string($id)) {
            throw new EuropaSDKException('The "customer_id" must be a string.');
        }

        $this->id = $id;
    }

    /**
     * Returns the Europa customer ID.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
