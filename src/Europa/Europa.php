<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa;

use Europa\Exceptions\EuropaSDKException;
use Europa\Helpers\EuropaSportCompetitionInsuranceHelper;

/**
 * Main class.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class Europa
{
    /**
     * @const string Version number of the Europa SDK for PHP.
     */
    const VERSION = '2.1.2';

    /**
     * @const string Default environment for requests.
     */
    const DEFAULT_ENVIRONMENT = 'prod';

    /**
     * @var EuropaCustomer The EuropaCustomer entity.
     */
    protected $customer;

    /**
     * @var EuropaClient The EuropaClient service.
     */
    protected $client;

    /**
     * @var string The environment we want to use.
     */
    protected $environment;

    /**
     * Instantiates a new Europa main object.
     *
     * @param array $config
     *
     * @throws EuropaSDKException
     */
    public function __construct(array $config = [])
    {
        $config += [
            'customer_id' => null,
            'environment' => static::DEFAULT_ENVIRONMENT,
        ];

        if (!$config['customer_id']) {
            throw new EuropaSDKException('Required "customer_id" key not supplied in config.');
        }

        $this->customer = new EuropaCustomer($config['customer_id']);
        $this->client = new EuropaClient();
        $this->environment = $config['environment'];
    }

    /**
     * Tests connection with the API and returns "echo" on success.
     *
     * @return string
     */
    public function echoApi()
    {
        $response = $this->send('GET', '/echo');

        return $response->getBody();
    }

    /**
     * Sends a request to the API and returns the response.
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $params
     *
     * @return EuropaResponse
     */
    public function send($method, $endpoint, array $params = [])
    {
        $request = new EuropaRequest(
            $this->customer,
            $method,
            $endpoint,
            $params,
            $this->environment
        );

        return $this->client->sendRequest($request);
    }

    /**
     * Returns the sport competition insurance helper.
     *
     * @return EuropaSportCompetitionInsuranceHelper
     */
    public function getSportCompetitionInsuranceHelper()
    {
        return new EuropaSportCompetitionInsuranceHelper(
            $this->customer,
            $this->client,
            $this->environment
        );
    }
}
