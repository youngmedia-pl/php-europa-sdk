<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa;

use Europa\Exceptions\EuropaSDKException;
use Europa\Http\EuropaHttpClient;

/**
 * Class EuropaClient.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaClient
{
    /**
     * @const string Production API URL.
     */
    const BASE_URL = 'https://api.tueuropa.pl';

    /**
     * @const string Development API URL.
     */
    const BASE_URL_DEV = 'https://apitest.tueuropa.pl';

    /**
     * @const int Request timeout in seconds.
     */
    const REQUEST_TIMEOUT = 20;

    /**
     * @var EuropaHttpClient The underlying HTTP client.
     */
    private $httpClient;

    /**
     * Instantiates a new EuropaClient service.
     */
    public function __construct()
    {
        $this->httpClient = new EuropaHttpClient();
    }

    /**
     * Sends a request to the API and returns the response.
     *
     * @param EuropaRequest $request
     *
     * @return EuropaResponse
     *
     * @throws EuropaSDKException
     */
    public function sendRequest(EuropaRequest $request)
    {
        $rawResponse = $this->httpClient->send(
            $request->getUrl(),
            $request->getMethod(),
            $request->getHeaders(),
            $request->getBody(),
            $request->getTimeout()
        );

        $response = new EuropaResponse($rawResponse);

        if ($response->isError()) {
            throw $response->getThrownException();
        }

        return $response;
    }
}
