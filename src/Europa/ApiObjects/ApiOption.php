<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiOption.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiOption extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'sub_options' => ApiOption::class,
    ];

    /**
     * Returns the code of the option.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getField('code');
    }

    /**
     * Returns the suboptions of the option.
     *
     * @return ApiList
     */
    public function getSubOptions()
    {
        return $this->getField('sub_options');
    }

    /**
     * Returns the value of the option.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->getField('value');
    }
}
