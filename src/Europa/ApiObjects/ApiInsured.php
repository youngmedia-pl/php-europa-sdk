<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiInsured.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiInsured extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'addons'  => ApiOption::class,
        'address' => ApiAddress::class,
        'data'    => ApiPerson::class,
        'options' => ApiOption::class,
    ];

    /**
     * Returns the additional attributes of the insured.
     *
     * @return ApiList
     */
    public function getAddons()
    {
        return $this->getField('addons');
    }

    /**
     * Returns the address of the insured.
     *
     * @return ApiAddress
     */
    public function getAddress()
    {
        return $this->getField('address');
    }

    /**
     * Returns the personal data of the insured.
     *
     * @return ApiPerson
     */
    public function getData()
    {
        return $this->getField('data');
    }

    /**
     * Returns the additional options selected by the insured.
     *
     * @return ApiList
     */
    public function getOptions()
    {
        return $this->getField('options');
    }
}
