<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiPossession.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPossession extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'address'    => ApiAddress::class,
        'attributes' => ApiOption::class,
    ];

    /**
     * Returns the address of the possession.
     *
     * @return ApiAddress
     */
    public function getAddress()
    {
        return $this->getField('address');
    }

    /**
     * Returns the addidtional attributes of the possession.
     *
     * @return ApiList
     */
    public function getAttributes()
    {
        return $this->getField('attributes');
    }

    /**
     * Returns the ID of the possession.
     *
     * @return string
     */
    public function getId()
    {
        return $this->getField('id');
    }

    /**
     * Returns the name of the possession.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns the type of the possession.
     *
     * @return string
     */
    public function getType()
    {
        return $this->getField('type');
    }

    /**
     * Returns the value of the possession.
     *
     * @return float
     */
    public function getValue()
    {
        return $this->getField('value');
    }
}
