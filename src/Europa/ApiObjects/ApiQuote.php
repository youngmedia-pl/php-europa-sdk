<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiQuote.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiQuote extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'details'           => ApiDetail::class,
        'payment_schema'    => ApiPaymentSchema::class,
        'premium'           => ApiPremium::class,
        'risks_definitions' => ApiRiskDefinition::class,
        'tariff_premium'    => ApiPremium::class,
    ];

    /**
     * Returns the description of the quote.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getField('description');
    }

    /**
     * Returns the short description of the quote.
     *
     * @return string
     */
    public function getDescriptionShort()
    {
        return $this->getField('description_short');
    }

    /**
     * Returns the details of the quote.
     *
     * @return ApiList
     */
    public function getDetails()
    {
        return $this->getField('details');
    }

    /**
     * Returns the name of the quote.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns the payment schema for this premium.
     *
     * @return ApiPaymentSchema
     */
    public function getPaymentSchema()
    {
        return $this->getField('payment_schema');
    }

    /**
     * Returns the premium of the quote.
     *
     * @return ApiPremium
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns the ID of the insurance product in the quote.
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->getField('product_id');
    }

    /**
     * Returns whether a valid promo code was used in this quote.
     *
     * @return bool
     */
    public function getPromoCodeValid()
    {
        return $this->getField('promo_code_valid');
    }

    /**
     * Returns the ID of the quote.
     *
     * @return string
     */
    public function getQuoteId()
    {
        return $this->getField('quote_id');
    }

    /**
     * Returns the risks defined in the quote.
     *
     * @return ApiList
     */
    public function getRisksDefinitions()
    {
        return $this->getField('risks_definitions');
    }

    /**
     * Returns the premium proposed for the quote in the tariff.
     *
     * @return ApiPremium
     */
    public function getTariffPremium()
    {
        return $this->getField('tariff_premium');
    }
}
