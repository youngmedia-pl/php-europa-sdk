<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;

/**
 * Class ApiPerson.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPerson extends ApiObject
{
    /**
     * Returns the date of the person birthday.
     *
     * @return DateTime
     */
    public function getBirthDate()
    {
        return $this->getField('birth_date');
    }

    /**
     * Returns the name of the person bussiness.
     *
     * @return string
     */
    public function getBusinessName()
    {
        return $this->getField('business_name');
    }

    /**
     * Returns the number of the person ID document.
     *
     * @return string
     */
    public function getDocumentNo()
    {
        return $this->getField('document_no');
    }

    /**
     * Returns the first name of the person.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->getField('first_name');
    }

    /**
     * Returns the last name of the person.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->getField('last_name');
    }

    /**
     * Returns the nationality of the person as ISO 3166-1 alpha-2 string.
     *
     * @return string
     */
    public function getNationality()
    {
        return $this->getField('nationality');
    }

    /**
     * Returns the NIP number of the person.
     *
     * @return string
     */
    public function getNip()
    {
        return $this->getField('nip');
    }

    /**
     * Returns the pesel number of the person.
     *
     * @return string
     */
    public function getPesel()
    {
        return $this->getField('pesel');
    }

    /**
     * Returns the REGON number of the person.
     *
     * @return string
     */
    public function getRegon()
    {
        return $this->getField('regon');
    }

    /**
     * Returns the short name of the person bussiness.
     *
     * @return string
     */
    public function getShortBusinessName()
    {
        return $this->getField('short_business_name');
    }

    /**
     * Returns the type of the person.
     *
     * enum {private, sole_trader, company, foreigner}
     *
     * @return string
     */
    public function getType()
    {
        return $this->getField('type');
    }
}
