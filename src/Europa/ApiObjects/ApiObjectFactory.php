<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;
use Europa\EuropaResponse;

/**
 * Class ApiObjectFactory.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiObjectFactory
{
    /**
     * @var array Fields casted to DateTime.
     */
    private static $dateTimeFields = [
        'birth_date',
        'date_rate',
        'due_date',
        'end_date',
        'payment_date',
        'policy_date',
        'start_date',
    ];

    /**
     * Creates object of a given class returned in the API response.
     *
     * @param EuropaResponse $response
     * @param string         $class
     *
     * @return ApiObject|ApiList
     */
    public function create(EuropaResponse $response, $class)
    {
        $data = $response->getDecodedBody();

        return $this->createApiObjectOrApiList($data, $class);
    }

    /**
     * Creates an ApiObject of a given class
     * or an ApiList of ApiObject's of a given class
     * whichever relevant depending on data layout.
     *
     * @param array  $data
     * @param string $class
     *
     * @return ApiObject|ApiList
     */
    private function createApiObjectOrApiList(array $data, $class)
    {
        if ($this->doesDataLayoutLookLikeApiList($data)) {
            return $this->createApiList($data, $class);
        }

        return $this->createApiObject($data, $class);
    }

    /**
     * Returns whether the data layout looks like an ApiList.
     *
     * @param array $data
     *
     * @return bool
     */
    private function doesDataLayoutLookLikeApiList(array $data)
    {
        return static::isEmptyArrayOrSequentialNumericalArray($data);
    }

    /**
     * Returns whether the data is an empty array or sequential numerical array.
     *
     * @param array $data
     *
     * @return bool
     */
    private static function isEmptyArrayOrSequentialNumericalArray(array $data)
    {
        return $data === [] || array_keys($data) === range(0, count($data) - 1);
    }

    /**
     * Creates an ApiObject of a given class.
     *
     * @param array  $data
     * @param string $class
     *
     * @return ApiObject
     */
    private function createApiObject(array $data, $class)
    {
        $fields = [];

        foreach ($data as $name => $field) {
            if ($this->isCompositeField($field)) {
                $fields[$name] = $this->createApiObjectCompositeField($name, $field, $class);
            } else {
                $fields[$name] = $this->createApiObjectPrimitiveField($name, $field);
            }
        }

        return new $class($fields);
    }

    /**
     * Returns whether the field of an ApiObject is a composite.
     *
     * @param mixed $field
     *
     * @return bool
     */
    private function isCompositeField($field)
    {
        return is_array($field);
    }

    /**
     * Creates a composite field of an ApiObject of a given class.
     *
     * @param string $name
     * @param array  $field
     * @param string $class
     *
     * @return ApiObject|ApiList|array
     */
    private function createApiObjectCompositeField($name, array $field, $class)
    {
        $fieldClass = $class::getFieldClass($name);

        if ($fieldClass !== null) {
            return $this->createApiObjectOrApiList($field, $fieldClass);
        }

        trigger_error('[php-europa-sdk] Unmapped field "' . $name . '" in API object "' . $class . '"', E_USER_NOTICE);

        return $field;
    }

    /**
     * Creates a primitive field of an ApiObject.
     *
     * @param string $name
     * @param mixed  $field
     *
     * @return mixed
     */
    private function createApiObjectPrimitiveField($name, $field)
    {
        if (in_array($name, static::$dateTimeFields)) {
            return new DateTime($field);
        }

        return $field;
    }

    /**
     * Creates an ApiList of ApiObject's of a given class.
     *
     * @param array  $data
     * @param string $class
     *
     * @return ApiList
     */
    private function createApiList(array $data, $class)
    {
        $items = [];

        foreach ($data as $item) {
            $items[] = $this->createApiObject($item, $class);
        }

        return new ApiList($items);
    }
}
