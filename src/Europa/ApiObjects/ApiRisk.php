<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;

/**
 * Class ApiRisk.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiRisk extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'possessions' => ApiPossession::class,
        'sum_insured' => ApiSumInsured::class,
    ];

    /**
     * Returns the code of the risk.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getField('code');
    }

    /**
     * Returns the date when the risk duration ends.
     *
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->getField('end_date');
    }

    /**
     * Returns the possessions included in the risk.
     *
     * @return ApiList
     */
    public function getPossessions()
    {
        return $this->getField('possessions');
    }

    /**
     * Returns the date when the risk duration starts.
     *
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->getField('start_date');
    }

    /**
     * Returns the sum insured in this risk.
     *
     * @return ApiSumInsured
     */
    public function getSumInsured()
    {
        return $this->getField('sum_insured');
    }
}
