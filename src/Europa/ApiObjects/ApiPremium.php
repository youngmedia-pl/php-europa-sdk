<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;

/**
 * Class ApiPremium.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPremium extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'payment_schema' => ApiPaymentSchema::class,
    ];

    /**
     * Returns the currency rate between the premium base and target currency.
     *
     * @return float
     */
    public function getCurrencyRate()
    {
        return $this->getField('currency_rate');
    }

    /**
     * Returns the date when the currency rate was checked.
     *
     * @return DateTime
     */
    public function getDateRate()
    {
        return $this->getField('date_rate');
    }

    /**
     * Returns the payment schema for this premium.
     *
     * @return ApiPaymentSchema
     */
    public function getPaymentSchema()
    {
        return $this->getField('payment_schema');
    }

    /**
     * Returns the value of the premium in the target currency.
     *
     * @return float
     */
    public function getValue()
    {
        return $this->getField('value');
    }

    /**
     * Returns the value of the premium in the base currency.
     *
     * @return float
     */
    public function getValueBase()
    {
        return $this->getField('value_base');
    }

    /**
     * Returns the base currency as ISO 4217 string.
     *
     * @return string
     */
    public function getValueBaseCurrency()
    {
        return $this->getField('value_base_currency');
    }

    /**
     * Returns the target currency as ISO 4217 string.
     *
     * @return string
     */
    public function getValueCurrency()
    {
        return $this->getField('value_currency');
    }
}
