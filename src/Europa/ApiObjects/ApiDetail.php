<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiDetail.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiDetail extends ApiObject
{
    /**
     * Returns the name of the detail.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns the value of the detail.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->getField('value');
    }
}
