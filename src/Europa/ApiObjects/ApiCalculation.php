<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;

/**
 * Class ApiCalculation.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiCalculation extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'premium'        => ApiPremium::class,
        'tariff_premium' => ApiPremium::class,
    ];

    /**
     * Returns the ID of the calculation.
     *
     * @return string
     */
    public function getCalculationId()
    {
        return $this->getField('calculation_id');
    }

    /**
     * Returns the checksum of the calculation.
     *
     * @return string
     */
    public function getChecksum()
    {
        return $this->getField('checksum');
    }

    /**
     * Returns the date due to the calculation is valid.
     *
     * @return DateTime
     */
    public function getDueDate()
    {
        return $this->getField('due_date');
    }

    /**
     * Returns the premium to be paid for the calculation.
     *
     * @return ApiPremium
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns whether a valid promo code was used in the calculation.
     *
     * @return bool
     */
    public function getPromoCodeValid()
    {
        return $this->getField('promo_code_valid');
    }

    /**
     * Returns the premium of the calculation as in the tariff.
     *
     * @return ApiPremium
     */
    public function getTariffPremium()
    {
        return $this->getField('tariff_premium');
    }
}
