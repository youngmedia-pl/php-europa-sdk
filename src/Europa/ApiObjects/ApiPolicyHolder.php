<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiPolicyHolder.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPolicyHolder extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'addons'     => ApiOption::class,
        'address'    => ApiAddress::class,
        'agreements' => ApiAgreement::class,
        'data'       => ApiPerson::class,
        'options'    => ApiOption::class,
    ];

    /**
     * Returns the additional attributes of the policy holder.
     *
     * @return ApiList
     */
    public function getAddons()
    {
        return $this->getField('addons');
    }

    /**
     * Returns the address of policy holder.
     *
     * @return ApiAddress
     */
    public function getAddress()
    {
        return $this->getField('address');
    }

    /**
     * Returns the agreements made by the policy holder.
     *
     * @return ApiList
     */
    public function getAgreements()
    {
        return $this->getField('agreements');
    }

    /**
     * Returns the personal details of the policy holder.
     *
     * @return ApiPerson
     */
    public function getData()
    {
        return $this->getField('data');
    }

    /**
     * Returns the email of the policy holder.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->getField('email');
    }

    /**
     * Returns the addidtional options selected by the policy holder.
     *
     * @return ApiList
     */
    public function getOptions()
    {
        return $this->getField('options');
    }

    /**
     * Returns the phone number of the policy holder.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->getField('telephone');
    }
}
