<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiPaymentSchema.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPaymentSchema extends ApiObject
{
}
