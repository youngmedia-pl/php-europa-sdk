<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiObject.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiObject extends Collection
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [];

    /**
     * Returns the value of a field or default if not set.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getField($name, $default = null)
    {
        return isset($this[$name]) ? $this[$name] : $default;
    }

    /**
     * Returns the ApiObject class the field is mapped to or null if none.
     *
     * @param string $name
     *
     * @return string|null
     */
    public static function getFieldClass($name)
    {
        return isset(static::$fieldMap[$name]) ? static::$fieldMap[$name] : null;
    }
}
