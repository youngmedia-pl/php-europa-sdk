<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiAgreement.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiAgreement extends ApiObject
{
    /**
     * Returns the code of the agreement.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getField('code');
    }

    /**
     * Returns the description of the agreement.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getField('description');
    }

    /**
     * Returns whether the agreement was accepted.
     *
     * @return bool
     */
    public function getValue()
    {
        return $this->getField('value');
    }
}
