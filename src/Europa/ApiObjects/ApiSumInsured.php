<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiSumInsured.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiSumInsured extends ApiObject
{
    /**
     * Returns the currency of the sum insured as ISO 4217 string.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->getField('currency');
    }

    /**
     * Returns the value of the sum insured.
     *
     * @return float
     */
    public function getValue()
    {
        return $this->getField('value');
    }
}
