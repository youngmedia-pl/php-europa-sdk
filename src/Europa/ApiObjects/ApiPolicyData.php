<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;

/**
 * Class ApiPolicyData.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPolicyData extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'addons'           => ApiOption::class,
        'configured_risks' => ApiRisk::class,
        'options'          => ApiOption::class,
        'sum_insured'      => ApiSumInsured::class,
    ];

    /**
     * Returns whether the insured was abroad when signing the policy.
     *
     * @return bool
     */
    public function getAbroad()
    {
        return $this->getField('abroad');
    }

    /**
     * Returns the additional attributes of the policy.
     *
     * @return ApiList
     */
    public function getAddons()
    {
        return $this->getField('addons');
    }

    /**
     * Returns the risks configured in the policy.
     *
     * @return ApiList
     */
    public function getConfiguredRisks()
    {
        return $this->getField('configured_risks');
    }

    /**
     * Returns the destiantion of the travel insured by the policy.
     *
     * Possible destinations are: a country, European Union or world.
     *
     * enum {country ISO 3166-1 alpha-2 code, EU, WR}
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->getField('destination');
    }

    /**
     * Returns the date the policy duration ends.
     *
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->getField('end_date');
    }

    /**
     * Returns the addidtional options selected in the policy.
     *
     * @return ApiList
     */
    public function getOptions()
    {
        return $this->getField('options');
    }

    /**
     * Returns the promo code used in the policy calculation.
     *
     * @return string
     */
    public function getPromoCode()
    {
        return $this->getField('promo_code');
    }

    /**
     * Returns the date the policy duration starts.
     *
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->getField('start_date');
    }

    /**
     * Returns the sum insured in the policy.
     *
     * @return ApiSumInsured
     */
    public function getSumInsured()
    {
        return $this->getField('sum_insured');
    }
}
