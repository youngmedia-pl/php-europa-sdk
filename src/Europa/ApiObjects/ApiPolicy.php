<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

use DateTime;

/**
 * Class ApiPolicy.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiPolicy extends ApiObject
{
    /**
     * @var array Maps field to ApiObject class.
     */
    protected static $fieldMap = [
        'cession'         => ApiCession::class,
        'data'            => ApiPolicyData::class,
        'divided_premium' => ApiPremium::class,
        'insureds'        => ApiInsured::class,
        'netto_premium'   => ApiPremium::class,
        'policy_holder'   => ApiPolicyHolder::class,
        'possessions'     => ApiPossession::class,
        'premium'         => ApiPremium::class,
        'salarieds'       => ApiSalary::class,
        'tariff_premium'  => ApiPremium::class,
    ];

    /**
     * Returns the cession in the policy.
     *
     * @return ApiCession
     */
    public function getCession()
    {
        return $this->getField('cession');
    }

    /**
     * Returns the checksum of the policy.
     *
     * @return string
     */
    public function getChecksum()
    {
        return $this->getField('checksum');
    }

    /**
     * Returns the details of the policy.
     *
     * @return ApiPolicyData
     */
    public function getData()
    {
        return $this->getField('data');
    }

    /**
     * Returns the premium divided.
     *
     * @return ApiPremium
     */
    public function getDividedPremium()
    {
        return $this->getField('divided_premium');
    }

    /**
     * Returns the insureds by the policy.
     *
     * @return ApiList
     */
    public function getInsureds()
    {
        return $this->getField('insureds');
    }

    /**
     * Returns the premium paid for the policy netto.
     *
     * @return ApiPremium
     */
    public function getNettoPremium()
    {
        return $this->getField('netto_premium');
    }

    /**
     * Returns the date when the policy was signed.
     *
     * @return DateTime
     */
    public function getPolicyDate()
    {
        return $this->getField('policy_date');
    }

    /**
     * Returns the policy holder.
     *
     * @return ApiPolicyHolder
     */
    public function getPolicyHolder()
    {
        return $this->getField('policy_holder');
    }

    /**
     * Returns the ID of the policy.
     *
     * @return string
     */
    public function getPolicyId()
    {
        return $this->getField('policy_id');
    }

    /**
     * Returns the number of the policy.
     *
     * @return string
     */
    public function getPolicyNumber()
    {
        return $this->getField('policy_number');
    }

    /**
     * Returns the possessions included in the policy.
     *
     * @return ApiList
     */
    public function getPossessions()
    {
        return $this->getField('possessions');
    }

    /**
     * Returns the premium paid for the policy.
     *
     * @return ApiPremium
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns the salarieds.
     *
     * @return ApiSalary
     */
    public function getSalarieds()
    {
        return $this->getField('salarieds');
    }

    /**
     * Returns the premium proposed for the policy in the tariff.
     *
     * @return ApiPremium
     */
    public function getTariffPremium()
    {
        return $this->getField('tariff_premium');
    }
}
