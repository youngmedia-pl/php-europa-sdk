<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiRiskDefinition.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiRiskDefinition extends ApiObject
{
    /**
     * Returns the code of the risk.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getField('code');
    }

    /**
     * Returns the currency of the sum insured in the risk as ISO 4217 string.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->getField('currency');
    }

    /**
     * Returns the maximal allowed sum insured in the risk.
     *
     * @return float
     */
    public function getMaxSu()
    {
        return $this->getField('max_su');
    }

    /**
     * Returns the minimal allowed sum insured in the risk.
     *
     * @return float
     */
    public function getMinSu()
    {
        return $this->getField('min_su');
    }

    /**
     * Returns the name of the risk.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns whether selecting this risk is optional.
     *
     * @return bool
     */
    public function getOptional()
    {
        return $this->getField('optional');
    }

    /**
     * Returns whether setting a possession in this risk is required.
     *
     * @return bool
     */
    public function getReqPossession()
    {
        return $this->getField('req_possession');
    }

    /**
     * Returns whether the date the risk duration starts can be modified.
     *
     * @return bool
     */
    public function getStartDateModifiable()
    {
        return $this->getField('start_date_modifiable');
    }

    /**
     * Returns whether the date the risk duration stops can be modified.
     *
     * @return bool
     */
    public function getStopDateModifiable()
    {
        return $this->getField('stop_date_modifiable');
    }

    /**
     * Returns whether the sum insured in the the risk can be modified.
     *
     * @return bool
     */
    public function getSuModifiable()
    {
        return $this->getField('su_modifiable');
    }

    /**
     * Returns the default value of the sum insured in the risk.
     *
     * @return float
     */
    public function getValue()
    {
        return $this->getField('value');
    }
}
