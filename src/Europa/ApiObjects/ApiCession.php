<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiCession.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiCession extends ApiObject
{
    /**
     * Returns the address of the cession.
     *
     * @return ApiAddress
     */
    public function getAddress()
    {
        return $this->getField('address');
    }

    /**
     * Returns the name of the cession.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns the NIP of the cession.
     *
     * @return string
     */
    public function getNip()
    {
        return $this->getField('nip');
    }

    /**
     * Returns the REGON of the cession.
     *
     * @return string
     */
    public function getRegon()
    {
        return $this->getField('regon');
    }
}
