<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiAddress.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiAddress extends ApiObject
{
    /**
     * Returns the city component of the address.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->getField('city');
    }

    /**
     * Returns the country component of the address as ISO 3166-1 alpha-2 string.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->getField('country');
    }

    /**
     * Returns the flat number component of the address.
     *
     * @return string
     */
    public function getFlatNo()
    {
        return $this->getField('flat_no');
    }

    /**
     * Returns the house number component of the address.
     *
     * @return string
     */
    public function getHouseNo()
    {
        return $this->getField('house_no');
    }

    /**
     * Returns the post code component of the address.
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->getField('post_code');
    }

    /**
     * Returns the street component of the address.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->getField('street');
    }
}
