<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\Helpers;

use DateTime;

/**
 * Class EuropaSportCompetitionInsuranceHelper.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
final class EuropaSportCompetitionInsuranceHelper extends EuropaInsuranceHelper
{
    /**
     * @var array Agreements code and description table.
     */
    private static $agreementsMap = [
        'zghand'   => 'Wyrażam zgodę na otrzymywanie od Grupy Ubezpieczeniowej Europa informacji handlowej drogą elektroniczną.',
        'zgmail'   => 'Wyrażam zgodę na otrzymywanie od ubezpieczyciela korespondencji za pośrednictwem adresu e-mail podanego przeze mnie w niniejszym wniosku o zawarcie umowy ubezpieczenia lub w złożonym oświadczeniu o zmianie danych do umowy ubezpieczenia oraz dodatkowo za pośrednictwem serwisu internetowego ubezpieczyciela.',
        'zgmark'   => 'Wyrażam zgodę na przetwarzanie moich danych osobowych w celach promocyjnych i marketingowych przez Grupę Ubezpieczeniową Europa, w skład której wchodzą TU Europa S.A. i TU na Życie Europa S.A. Zostałem(am) poinformowany(a), że przysługuje mi prawo dostępu do treści moich danych osobowych oraz ich poprawiania. Podanie danych jest dobrowolne.',
        'zgmbez'   => 'Wyrażam zgodę na używanie przez Grupę Ubezpieczeniową Europa telekomunikacyjnych urządzeń końcowych i automatycznych systemów wywołujących dla celów marketingu bezpośredniego.',
        'zgobj'    => 'Wyrażam zgodę na objęcie mnie ochroną ubezpieczeniową przez Towarzystwo Ubezpieczeń Europa S.A.',
        'zgpotw'   => 'Potwierdzam otrzymanie i zapoznanie się z Ogólnymi Warunkami Ubezpieczenia przed zawarciem umowy ubezpieczenia oraz ich zrozumienie i akceptację.',
        'zgregods' => 'Oświadczam, że zapoznałem(am) się z Regulaminem świadczenia usług drogą elektroniczną i go akceptuję. Oświadczam, że zostałem(am) poinformowany(a) o prawie odstąpienia od umowy ubezpieczenia w ciągu 30 dni od daty jej zawarcia. Z tym, że w przypadku złożenia oświadczenia o zawarciu umowy ubezpieczenia z zastosowaniem środków porozumiewania się na odległość termin ten liczony jest od dnia otrzymania potwierdzenia zawarcia umowy ubezpieczenia lub od dnia potwierdzenia informacji, o którym mowa w art. 39 ust. 3 ustawy o prawach konsumenta.',
    ];

    /**
     * @inheritdoc
     */
    public function getQuotes(array $params)
    {
        $params = $this->compileQuotesParams($params);

        return parent::getQuotes($params);
    }

    /**
     * @inheritdoc
     */
    public function makeCalculation(array $params)
    {
        $params = $this->compileCalculationParams($params);

        return parent::makeCalculation($params);
    }

    /**
     * @inheritdoc
     */
    public function issuePolicy(array $params)
    {
        $params = $this->compilePolicyParams($params);

        return parent::issuePolicy($params);
    }

    /**
     * @inheritdoc
     */
    public function printPolicy(array $params)
    {
        $params = $this->compilePrintParams($params);

        return parent::printPolicy($params);
    }

    /**
     * Compiles request parameters into endpoint-friendly format.
     *
     * @param array $params
     *
     * @return array
     */
    private function compileQuotesParams(array $params)
    {
        return [
            'data'       => $this->policyData($params, false),
            'prepersons' => [
                $this->preperson($params),
            ],
        ];
    }

    /**
     * Compiles request parameters into endpoint-friendly format.
     *
     * @param array $params
     *
     * @return array
     */
    private function compileCalculationParams(array $params)
    {
        return [
            'data'          => $this->policyData($params, true),
            'insureds'      => [
                $this->insured($params),
            ],
            'policy_holder' => $this->policyHolder($params),
            'product_id'    => $params['product_id'],
        ];
    }

    /**
     * Compiles request parameters into endpoint-friendly format.
     *
     * @param array $params
     *
     * @return array
     */
    private function compilePolicyParams(array $params)
    {
        return [
            'calculation_id' => $params['calculation_id'],
            'checksum'       => $params['calculation_checksum'],
            'payment_date'   => $this->date($params['payment_date']),
            'policy_date'    => $this->date($params['policy_date']),
            'solicitors'     => [
                $this->solicitor($params),
            ],
        ];
    }

    /**
     * Compiles request parameters into endpoint-friendly format.
     *
     * @param array $params
     *
     * @return array
     */
    private function compilePrintParams(array $params)
    {
        return [
            'checksum'  => $params['policy_checksum'],
            'policy_id' => $params['policy_id'],
        ];
    }

    /**
     * Makes policy data.
     *
     * @param array $params
     * @param bool  $isCalculationRequest
     *
     * @return array
     */
    private function policyData(array $params, $isCalculationRequest)
    {
        $policyData = [
            'configured_risks' => [
                $this->resignationRisk($params),
            ],
            'end_date'         => $this->endDate($params['competition_end_date']),
            'start_date'       => $this->startDate($params['competition_start_date']),
            'sum_insured'      => $this->sumInsured($params['sum_insured']),
        ];

        if ($isCalculationRequest) {
            $policyData['addons'] = [
                $this->eventNameAddon($params),
            ];
        }

        return $policyData;
    }

    /**
     * Makes sum insured.
     *
     * @param string $value
     *
     * @return array
     */
    private function sumInsured($value)
    {
        return [
            'currency' => 'PLN',
            'value'    => floatval($value),
        ];
    }

    /**
     * Makes event name addon.
     *
     * @param array $params
     *
     * @return array
     */
    private function eventNameAddon(array $params)
    {
        return [
            'code'  => 'EVENT_NAME',
            'value' => $params['competition_name'],
        ];
    }

    /**
     * Makes resignation risk.
     *
     * @param array $params
     *
     * @return array
     */
    private function resignationRisk(array $params)
    {
        return [
            'code'        => 'KR',
            'end_date'    => $this->endDate($params['competition_end_date']),
            'start_date'  => $this->startDate('tomorrow'),
            'sum_insured' => $this->sumInsured($params['participation_fee']),
        ];
    }

    /**
     * Makes preperson.
     *
     * @param array $params
     *
     * @return array
     */
    private function preperson(array $params)
    {
        return [
            'birth_date' => $this->birthDate($params['participant_birth_date']),
            'options'    => [
                $this->sportTypeOption($params),
            ],
        ];
    }

    /**
     * Makes sport type option.
     *
     * @param array $params
     *
     * @return array
     */
    private function sportTypeOption(array $params)
    {
        return [
            'code'  => 'SPORT_TYPE',
            'value' => $params['is_professional'] ? 'zawodowy' : 'amatorski',
        ];
    }

    /**
     * Makes policy holder.
     *
     * @param array $params
     *
     * @return array
     */
    private function policyHolder(array $params)
    {
        return [
            'address'    => $this->personAddress($params),
            'agreements' => $this->agreements($params),
            'data'       => $this->personData($params),
            'email'      => $params['participant_email'],
            'telephone'  => $params['participant_phone'],
        ];
    }

    /**
     * Makes person data.
     *
     * @param array $params
     *
     * @return array
     */
    private function personData(array $params)
    {
        $personData = [
            'birth_date'  => $this->birthDate($params['participant_birth_date']),
            'first_name'  => $params['participant_first_name'],
            'last_name'   => $params['participant_last_name'],
            'nationality' => strtoupper($params['participant_country']),
        ];

        if (strtolower($params['participant_country']) == 'pl') {
            $personData += [
                'pesel' => $params['participant_pesel'],
                'type'  => 'private',
            ];
        } else {
            $personData += [
                'document_no'   => $params['participant_passport'],
                'document_type' => 'PA',
                'sex'           => $params['participant_sex'],
                'type'          => 'foreigner',
            ];
        }

        return $personData;
    }

    /**
     * Makes person address.
     *
     * @param array $params
     *
     * @return array
     */
    private function personAddress(array $params)
    {
        $personAddress = [
            'city'      => $params['participant_city'],
            'country'   => strtoupper($params['participant_country']),
            'house_no'  => $params['participant_house_number'],
            'post_code' => $params['participant_zipcode'],
            'street'    => $params['participant_street'],
        ];

        if ($params['participant_flat_number']) {
            $personAddress['flat_no'] = $params['participant_flat_number'];
        }

        return $personAddress;
    }

    /**
     * Makes agreements.
     *
     * @param array $params
     *
     * @return array
     */
    private function agreements(array $params)
    {
        $agreements = [];

        foreach (static::$agreementsMap as $code => $description) {
            $agreements[] = [
                'code'        => strtoupper($code),
                'description' => $description,
                'value'       => in_array($code, $params['agreements']),
            ];
        }

        return $agreements;
    }

    /**
     * Makes insured.
     *
     * @param array $params
     *
     * @return array
     */
    private function insured(array $params)
    {
        return [
            'address' => $this->personAddress($params),
            'data'    => $this->personData($params),
            'options' => [
                $this->sportTypeOption($params),
            ],
        ];
    }

    /**
     * Makes solicitor.
     *
     * @param array $params
     *
     * @return array
     */
    private function solicitor(array $params)
    {
        return [
            'agent_id'     => $params['agent_id'],
            'permissions'  => [
                'ZAW',
            ],
            'solicitor_id' => $params['solicitor_id'],
        ];
    }

    /**
     * Makes start date.
     *
     * @param string $time
     *
     * @return string
     */
    private function startDate($time)
    {
        return $this->dateTime($time)->modify('midnight')->format('c');
    }

    /**
     * Makes end date.
     *
     * @param string $time
     *
     * @return string
     */
    private function endDate($time)
    {
        return $this->dateTime($time)->modify('tomorrow -1 sec')->format('c');
    }

    /**
     * Makes date.
     *
     * @param string $time
     *
     * @return string
     */
    private function date($time)
    {
        return $this->dateTime($time)->format('c');
    }

    /**
     * Makes birth date.
     *
     * @param string $time
     *
     * @return string
     */
    private function birthDate($time)
    {
        return $this->dateTime($time)->format('Y-m-d');
    }

    /**
     * Instatiates a new DateTime object using beginning of Unix epoch instead of `now` as default value.
     *
     * @param string $time
     *
     * @return DateTime
     */
    private function dateTime($time = '@0')
    {
        return new DateTime($time ?: '@0');
    }
}
