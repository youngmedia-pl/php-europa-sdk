<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\Helpers;

use Europa\ApiObjects\ApiCalculation;
use Europa\ApiObjects\ApiList;
use Europa\ApiObjects\ApiPolicy;
use Europa\Europa;
use Europa\EuropaCustomer;
use Europa\EuropaClient;
use Europa\EuropaRequest;
use Europa\EuropaResponse;

/**
 * Class EuropaInsuranceHelper.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaInsuranceHelper
{
    /**
     * @var EuropaCustomer The EuropaCustomer entity.
     */
    private $customer;

    /**
     * @var EuropaClient The EuropaClient service.
     */
    private $client;

    /**
     * @var string The environment we want to use.
     */
    private $environment;

    /**
     * Instantiates a new EuropaInsuranceHelper helper.
     *
     * @param EuropaCustomer $customer
     * @param EuropaClient   $client
     * @param string         $environment
     */
    public function __construct(EuropaCustomer $customer, EuropaClient $client, $environment = Europa::DEFAULT_ENVIRONMENT)
    {
        $this->customer = $customer;
        $this->client = $client;
        $this->environment = $environment;
    }

    /**
     * Gets quotes from the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return ApiList
     */
    public function getQuotes(array $params)
    {
        $response = $this->send('POST', '/quotes', $params);

        return $response->getQuotes();
    }

    /**
     * Makes calculation via the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return ApiCalculation
     */
    public function makeCalculation(array $params)
    {
        $response = $this->send('POST', '/policies/calculate', $params);

        return $response->getCalculation();
    }

    /**
     * Issues policy via the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return ApiPolicy
     */
    public function issuePolicy(array $params)
    {
        $response = $this->send('POST', '/policies/issue', $params);

        return $response->getPolicy();
    }

    /**
     * Prints policy via the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return PDF as string
     */
    public function printPolicy(array $params)
    {
        $response = $this->send('POST', '/policies/print', $params);

        return $response->getBody();
    }

    /**
     * Sends a request to the API and returns the response.
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $params
     *
     * @return EuropaResponse
     */
    public function send($method, $endpoint, array $params = [])
    {
        $request = new EuropaRequest(
            $this->customer,
            $method,
            '/insurance/v2' . $endpoint,
            $params,
            $this->environment
        );

        return $this->client->sendRequest($request);
    }
}
