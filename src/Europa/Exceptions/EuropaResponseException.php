<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\Exceptions;

use Europa\EuropaResponse;

/**
 * Class EuropaResponseException.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaResponseException extends EuropaSDKException
{
    /**
     * Creates a response exception based on the API response.
     *
     * @param EuropaResponse $response
     *
     * @return EuropaResponseException
     */
    public static function create(EuropaResponse $response)
    {
        $code = $response->getHttpStatusCode();
        $message = $response->getBody();

        switch ($code) {
            // Validation error.
            case 400:
            case 422:
            case 423:
                return new static(new EuropaValidationException($message, $code));

            // Authentication error.
            case 401:
                return new static(new EuropaAuthenticationException($message, $code));

            // Missing permissions.
            case 403:
                return new static(new EuropaAuthorizationException($message, $code));

            // Bad request.
            case 404:
            case 406:
                return new static(new EuropaRequestException($message, $code));

            // Server issue.
            case 500:
                return new static(new EuropaServerException($message, $code));

            // Other errors.
            default:
                return new static(new EuropaOtherException($message, $code));
        }
    }

    /**
     * Instantiates a new EuropaResponseException object.
     *
     * @param EuropaSDKException $detailedException
     */
    public function __construct(EuropaSDKException $detailedException)
    {
        parent::__construct($detailedException->getMessage(), $detailedException->getCode(), $detailedException);
    }
}
