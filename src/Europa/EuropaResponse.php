<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa;

use Europa\ApiObjects\ApiCalculation;
use Europa\ApiObjects\ApiList;
use Europa\ApiObjects\ApiObjectFactory;
use Europa\ApiObjects\ApiPolicy;
use Europa\ApiObjects\ApiQuote;
use Europa\Exceptions\EuropaResponseException;

/**
 * Class EuropaResponse.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaResponse
{
    /**
     * @var array The headers of the API response.
     */
    private $headers;

    /**
     * @var string The body of the API response.
     */
    private $body;

    /**
     * @var array|null The decoded body of the API response.
     */
    private $decodedBody;

    /**
     * @var int The HTTP status code of the API response.
     */
    private $httpStatusCode;

    /**
     * @var EuropaResponseException|null The exception thrown by the request.
     */
    private $thrownException;

    /**
     * Instantiates a new EuropaResponse entity.
     *
     * @param string $rawResponse
     */
    public function __construct($rawResponse)
    {
        $this->parse($rawResponse);

        if ($this->isError()) {
            $this->createException();
        }
    }

    /**
     * Parses raw HTTP response.
     *
     * @param string $rawResponse
     */
    private function parse($rawResponse)
    {
        // Separate headers and body.
        $parts = explode("\r\n\r\n", $rawResponse);

        // Get response body.
        $this->body = array_pop($parts);
        $this->decodedBody = json_decode($this->body, true);

        // Get response headers.
        // There will be multiple headers if a 301 was followed, etc. We want the last one.
        $rawHeaders = array_pop($parts);

        // Parse response headers.
        $this->headers = [];
        $this->httpStatusCode = null;

        foreach (explode("\r\n", $rawHeaders) as $rawHeader) {
            if (strpos($rawHeader, ': ') === false) {
                $detectHttpStatusCode = '|HTTP/\d\.\d\s+(\d+)\s+.*|';
                preg_match($detectHttpStatusCode, $rawHeader, $match);
                $this->httpStatusCode = (int) $match[1];
            } else {
                list($key, $value) = explode(': ', $rawHeader, 2);
                $this->headers[$key] = $value;
            }
        }
    }

    /**
     * Returns whether the API returned an error.
     *
     * @return bool
     */
    public function isError()
    {
        $successHttpStatusCodes = [200, 201];

        return !in_array($this->httpStatusCode, $successHttpStatusCodes);
    }

    /**
     * Creates an exception to be thrown later.
     */
    private function createException()
    {
        $this->thrownException = EuropaResponseException::create($this);
    }

    /**
     * Returns the headers of the response.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Returns the body of the response.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Returns the decoded body of the response.
     *
     * @return array|null
     */
    public function getDecodedBody()
    {
        return $this->decodedBody;
    }

    /**
     * Return the HTTP status code of the response.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * Returns the exception thrown by this request.
     *
     * @return EuropaResponseException|null
     */
    public function getThrownException()
    {
        return $this->thrownException;
    }

    /**
     * Convenience method for creating an ApiList of ApiQuote objects.
     *
     * @return ApiList
     */
    public function getQuotes()
    {
        $factory = new ApiObjectFactory();

        return $factory->create($this, ApiQuote::class);
    }

    /**
     * Convenience method for creating an ApiCalculation object.
     *
     * @return ApiCalculation
     */
    public function getCalculation()
    {
        $factory = new ApiObjectFactory();

        return $factory->create($this, ApiCalculation::class);
    }

    /**
     * Convenience method for creating an ApiPolicy object.
     *
     * @return ApiPolicy
     */
    public function getPolicy()
    {
        $factory = new ApiObjectFactory();

        return $factory->create($this, ApiPolicy::class);
    }
}
