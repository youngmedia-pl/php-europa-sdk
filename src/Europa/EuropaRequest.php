<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa;

/**
 * Class EuropaRequest.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaRequest
{
    /**
     * @var EuropaCustomer The EuropaCustomer entity.
     */
    private $customer;

    /**
     * @var string The HTTP method of the request.
     */
    private $method;

    /**
     * @var string The API endpoint of the request.
     */
    private $endpoint;

    /**
     * @var array The parameters of the request.
     */
    private $params;

    /**
     * @var string The environment of the request.
     */
    private $environment;

    /**
     * Instantiates a new EuropaRequest entity.
     *
     * @param EuropaCustomer $customer
     * @param string         $method
     * @param string         $endpoint
     * @param array          $params
     * @param string         $environment
     */
    public function __construct(EuropaCustomer $customer, $method, $endpoint, array $params = [], $environment = Europa::DEFAULT_ENVIRONMENT)
    {
        $this->customer = $customer;
        $this->method = $method;
        $this->endpoint = $endpoint;
        $this->params = $params;
        $this->environment = $environment;
    }

    /**
     * Returns the URL of the request.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getBaseUrl() . $this->getRelativeUrl();
    }

    /**
     * Returns the base URL of the request.
     *
     * @return string
     */
    private function getBaseUrl()
    {
        return $this->environment === 'prod' ? EuropaClient::BASE_URL : EuropaClient::BASE_URL_DEV;
    }

    /**
     * Returns the relative URL of the request.
     *
     * @return string
     */
    private function getRelativeUrl()
    {
        return $this->endpoint . '?' . http_build_query($this->getUrlParams(), null, '&');
    }

    /**
     * Returns the parameters to be sent in the URL of the request.
     *
     * @return array
     */
    private function getUrlParams()
    {
        $urlParams = $this->method !== 'POST' ? $this->params : [];

        return $urlParams + [
            'customer_id' => $this->customer->getId(),
        ];
    }

    /**
     * Returns the HTTP method of the request.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Returns the headers of the request.
     *
     * @return array
     */
    public function getHeaders()
    {
        return [
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * Returns the body of the request.
     *
     * @return string
     */
    public function getBody()
    {
        return json_encode($this->getBodyParams());
    }

    /**
     * Returns the parameters to be sent in the body of the request.
     *
     * @return array
     */
    private function getBodyParams()
    {
        return $this->method === 'POST' ? $this->params : [];
    }

    /**
     * Returns the timeout of the request.
     *
     * @return int
     */
    public function getTimeout()
    {
        return EuropaClient::REQUEST_TIMEOUT;
    }
}
