<?php

/**
 * This file is part of the Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\Http;

use Europa\Exceptions\EuropaSDKException;

/**
 * Class EuropaHttpClient.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaHttpClient
{
    /**
     * @var resource A curl instance.
     */
    private $curl;

    /**
     * @var string The last HTTP response from the server.
     */
    private $response;

    /**
     * @var int The last error code.
     */
    private $errorCode;

    /**
     * @var string The last error message.
     */
    private $errorMessage;

    /**
     * Sends a request to the server and returns the response.
     *
     * @param string $url
     * @param string $method
     * @param array  $headers
     * @param string $body
     * @param int    $timeout
     *
     * @return string
     *
     * @throws EuropaSDKException
     */
    public function send($url, $method, array $headers, $body, $timeout)
    {
        $this->openConnection($url, $method, $headers, $body, $timeout);

        $this->executeConnection();

        $this->closeConnection();

        if ($this->errorCode) {
            throw new EuropaSDKException($this->errorMessage, $this->errorCode);
        }

        return $this->response;
    }

    /**
     * Opens a new curl connection.
     *
     * @param string $url
     * @param string $method
     * @param array  $headers
     * @param string $body
     * @param int    $timeout
     */
    private function openConnection($url, $method, array $headers, $body, $timeout)
    {
        $options = [
            CURLOPT_CAINFO         => __DIR__ . '/europa.pem',
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HEADER         => true,
            CURLOPT_HTTPHEADER     => $this->compileRequestHeaders($headers),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_TIMEOUT        => $timeout,
            CURLOPT_URL            => $url,
        ];

        if ($method !== 'GET') {
            $options[CURLOPT_POSTFIELDS] = $body;
        }

        $this->curl = curl_init();
        curl_setopt_array($this->curl, $options);
    }

    /**
     * Executes the curl connection.
     */
    private function executeConnection()
    {
        $this->response = curl_exec($this->curl);
        $this->errorCode = curl_errno($this->curl);
        $this->errorMessage = curl_error($this->curl);
    }

    /**
     * Closes the curl connection.
     */
    private function closeConnection()
    {
        curl_close($this->curl);
    }

    /**
     * Compiles request headers into a curl-friendly format.
     *
     * @param array $headers
     *
     * @return array
     */
    private function compileRequestHeaders(array $headers)
    {
        $compiledHeaders = [];

        foreach ($headers as $key => $value) {
            $compiledHeaders[] = $key . ': ' . $value;
        }

        return $compiledHeaders;
    }
}
