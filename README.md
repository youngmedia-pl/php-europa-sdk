# Europa SDK for PHP
---
This repository contains the PHP SDK that allows you to access the [TU Europa](https://tueuropa.pl/) API from your PHP project.

## Installation
---
The Europa PHP SDK can be installed with [Composer](https://getcomposer.org/). Run this command:
```
composer require youngmedia-pl/europa-sdk
```

## Usage
---
Simple example of getting quotes.

```php
<?php

require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',   // the private customer identifier
    'environment' => 'dev',             // change to 'prod' for production environment (default)
]);

// Use our helper class for sport competition insurance.
$helper = $europa->getSportCompetitionInsuranceHelper();

// Get quotes.
try {
    $quotes = $helper->getQuotes([
        'competition_end_date'      => '2024-12-31',
        'competition_start_date'    => '2024-12-31',
        'is_professional'           => false,
        'participant_birth_date'    => '1950-12-31',
        'participation_fee'         => '60.99',
        'sum_insured'               => '30000',
    ]);
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues.
    echo 'Europa SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($quotes);
```
