# Get quotes
---
This example shows how to get insurance quotes using Europa API and SDK for PHP.

An insurance quote is a preliminary insurance offer that can be presented to the client at the beginning of purchase. To get quotes from the API just a basic set of data is required. The API may return one or more quote(s) depending on the submitted data and/or your Europa customer configuration. Each quote may differ in price and included insurance options like insurance of resignation cost or insurance of rehabilitation cost. The quote helps client decide which insurance product to choose. The final insurance price must be calculated later on after client fills in all data using `makeCalculation()` API method.

If the returned quotes list is empty or some quotes that were supposed to be are missing, please make sure you submitted correct `customer_id` parameter and if yes, contact Europa sales department so that they can fix your customer configuration.

## Example for sport competition insurance
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

// Get quotes.
try {
    $quotes = $helper->getQuotes([
        'competition_end_date'      => '2024-12-31',    // the end date of the competition
        'competition_start_date'    => '2024-12-31',    // the start date of the competition
        'is_professional'           => false,           // whether the participant is a professional sportsman
        'participant_birth_date'    => '1950-12-31',    // the birth date of the participant
        'participation_fee'         => '60.99',         // the fee of the participation only
        'sum_insured'               => '30000',         // sum insured (NNW)
    ]);
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues.
    echo 'Europa SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($quotes);
```
