# Insurance Form
---
This example shows how to create an insurance form with Europa API using SDK for PHP and JavaScript.

The JavaScript SDK creates an insurance form that collects the client data and sends it to a server pointed by `quotesUrl` parameter. The response to this call should contain a JSON-encoded quotes fetched by `getQuotes()` PHP SDK method. Then the JavaScript SDK presents the quotes to the client so that he/she can pick up the one he/she is interested in. The selected quote will be send to the server on submit.

## Example
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

// If quotes requested.
if ($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {

    // Get quotes.
    try {
        $quotes = $helper->getQuotes([
            'competition_end_date'      => $_GET['competition_date'],
            'competition_start_date'    => $_GET['competition_date'],
            'is_professional'           => $_GET['is_professional'],
            'participant_birth_date'    => $_GET['participant_birth_date'],
            'participation_fee'         => $_GET['participation_fee'],
            'sum_insured'               => $_GET['sum_insured'],
        ]);
    } catch (Europa\Exceptions\EuropaResponseException $e) {
        // When API returns an error.
        $error = 'API returned an error: ' . $e->getMessage();
    } catch (Europa\Exceptions\EuropaSDKException $e) {
        // Other issues.
        $error = 'Europa SDK returned an error: ' . $e->getMessage();
    }

    if ($error) {
        echo json_encode(['error' => $error]);
        exit;
    }

    // Print details.
    echo json_encode($quotes->asArray());
    exit;
}
?>

<html>
<head>
    <!-- The form stylesheet can be downloaded and customized as needed. -->
    <link rel="stylesheet" type="text/css" href="https://dostartu.pl/cdn/europa2.css">
</head>
<body>
    <button id="submit">Submit</button>

    <!-- This is the HTML root element where the form will be displayed. -->
    <div id="insurance_form"></div>

    <!-- jQuery dependency. -->
    <script
        src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
        crossorigin="anonymous"></script>

    <!-- The form API. -->
    <script type="text/javascript" src="https://dostartu.pl/cdn/europa2.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            // Customize the form and grab API.
            var europa2 = $('#insurance_form').Europa2({
                quotesUrl: '{quotes-url}',                  // url where `getQuotes()` PHP-SDK method is called, in this example this would be the same file
                baseInsuranceName: 'Package 1',             // name of the insurance package containt basic offer
                resignationInsuranceName: 'Package 2',      // name of the Europa insurance package with resignation risk
                rehabilitationInsuranceName: 'Package 3',   // name of the T.U. Europa insurance package with rehabilitation risk
                participantFirstName: false,                // a complete list of possible form inputs, false values are default and can be ommitted
                participantLastName: false,
                participantBirthDate: false,
                participantPesel: true,                     // pesel is to be displayed
                participantEmail: false,
                participantPhone: false,
                participantCountry: false,
                participantCity: false,
                participantZipcode: false,
                participantStreet: false,
                participantHouseNumber: false,
                participantFlatNumber: false,
            }).Europa2();

            // In this example we disable submit button until quotes request is done.
            $('#submit').prop('disabled', true);

            // Request quotes. This is a separate method, so that the form can be initialized once,
            // and the quotes can be dynamically fetched whenever relevant eg. when any of below listed values changes.
            europa2.getQuotes(
                '30000',        // sum insured
                '60.99',        // participation fee
                '2024-12-31',   // competition date
                '1950-12-31',   // participant birth date
                function () {
                // When done enable submit button.
                $('#submit').prop('disabled', false);
            });

            // When submit button is clicked.
            $('#submit').on('click', function () {
                // Validate form and submit.
                if (europa2.validate('1950-12-31', 'male')) {
                    europa2.submit();
                }
            });

            // At any time selected quote can be accessed using `selectedQuote()` API method.
            $('input').on('input', function () {
                console.log(europa2.selectedQuote());
            });
        });
    </script>
</body>
</html>
```
