# Echo API
---
This example shows how to test connection with Europa API using SDK for PHP.

Before you try to connect with the API, please contact with the Europa IT department to enable your server IP for incoming connections to the selected development or production environment.

## Example
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',
]);

// Echo API.
try {
    $echo = $europa->echoApi();
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues.
    echo 'Europa SDK returned an error: ' . $e->getMessage();
    exit;
}

// Prints "echo" on success.
var_dump($echo);
exit;
```
