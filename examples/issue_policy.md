# Issue policy
---
This example shows how to issue an insurance policy using Europa API and SDK for PHP.

An insurance policy is issued from an insurance calculation pointed by the ID and checksum. The policy data will be set from the data submitted to that calculation. Your system should perform proper checks before calling this method eg. whether the participant has already payed for the calculation, because issuing an insurance policy in a production environment has legal consequences. The returned policy can be stored in your database, but you don't need to print the policy to a PDF file neither send it to the client, as the API does this automatically after calling this method (the client e-mail address is retrieved from the calculation data).

## Example for sport competition insurance
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

// Issue policy.
try {
    $policy = $helper->issuePolicy([
        'agent_id'              => '{agent-id}',                // the ID of the insurance agent signing the policy, usually provided to you by Europa representatives
        'calculation_checksum'  => '{calculation-checksum}',    // the checksum of the calculation that was payed by the participant
        'calculation_id'        => '{calculation-id}',          // the ID of the calculation that was payed by the participant
        'payment_date'          => '2017-12-31 23:59:59',       // the date when the payment was settled in a format accepted by PHP DateTime class
        'policy_date'           => '2017-12-31 23:59:59',       // the date when the policy was signed (usually equals to payment date) in a format accepted by PHP DateTime class
        'solicitor_id'          => '{solicitor-id}',            // the ID of the solictor selling the policy, usually provided to you by Europa representatives
    ]);
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues.
    echo 'Europa SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($policy);
```
