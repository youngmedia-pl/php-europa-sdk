# Make calculation
---
This example shows how to make a calculation using Europa API and SDK for PHP.

Insurance calculation contains final insurance price and thus requires a precise information. This method should be called in the summary, namely after the client chooses an insurance offer and fills up required data but before purchase. The returned calculation ID and checksum should be stored in your database until client finishes the purchase and then passed to `issuePolicy()` API method. If the purchase did not succeed or at any other reason the client visits the summary page once more, the previous calculation should not be reused and a new calculation should be made, as the data submitted to the calculation is likely to change in time and each calculation has limited validity time expressed in `due_time` field.

If the client is not a polish citizen, passport and sex fields are required instead of pesel.

Possible agreements codes checked by the client have following meaning (agreements marked with an 'X' are required):  
( ) zghand      - agreement for receiving advertisement via e-mail,  
( ) zgmail      - agreement for receiving insurance policy via e-mail,  
( ) zgmark      - agreement for data processing,  
( ) zgmbez      - agreement for receiving advertisement via phone,  
(X) zgobj       - agreement for insurance,  
(X) zgpotw      - agreement for regulation OWU,  
(X) zgregods    - agreement for regulation of cancellation the insurance.

## Example for sport competition insurance
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

// Make calculation.
try {
    $calculation = $helper->makeCalculation([
        'agreements'                => ['zgobj', 'zgpotw', 'zgregods'], // list of agreements checked by the participant
        'competition_end_date'      => '2024-12-31',        // the end date of the competition
        'competition_name'          => 'Foo competition',   // the name of the competition
        'competition_start_date'    => '2024-12-31',        // the start date of the competition
        'is_professional'           => false,               // whether the participant is a professional sportsman
        'participant_birth_date'    => '1950-12-31',        // the birth date of the participant
        'participant_city'          => 'City',              // the city part of participant address
        'participant_country'       => 'pl',                // the country part of participant address as alpha-2 code
        'participant_email'         => 'mail@example.com',  // the e-mail address to send the policy to
        'participant_first_name'    => 'John',              // the participant first name
        'participant_flat_number'   => '3a',                // the flat number part of participant address
        'participant_house_number'  => '2/1',               // the house number part of participant address
        'participant_last_name'     => 'Doe',               // the participant last name
        // 'participant_passport'   => 'PPP309482',         // the passport number of participant, to be set if participant is a foreigner
        'participant_pesel'         => '50123100011',       // the pesel of participant, to be set if participant is a polish
        'participant_phone'         => '+48123123123',      // the participant phone number with country prefix
        // 'participant_sex'        => 'male',              // the sex of participant, to be set if participant is a foreigner ('male' or 'female')
        'participant_street'        => 'Street',            // the street part of participant address
        'participant_zipcode'       => '00-001',            // the zip code part of participant address
        'participation_fee'         => '60.99',             // the fee of the participation only
        'product_id'                => '{product-id}',      // the insurance product ID from the insurance quote selected by the participant
        'sum_insured'               => '30000',             // the sum insured (NNW)
    ]);
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues.
    echo 'Europa SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($calculation);
```
