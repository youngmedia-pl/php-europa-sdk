# Print policy
---
This example shows how to print a policy to a PDF file using Europa API and SDK for PHP.

This method can be used to print a duplicate of the policy. The original policy is printed and sent to the client automatically after calling `issuePolicy()` API method.

## Example for sport competition insurance
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}',
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

// Print policy.
try {
    $pdf = $helper->printPolicy([
        'policy_checksum'   => '{policy-checksum}', // the checksum of the policy
        'policy_id'         => '{policy-id}',       // the id of the policy
    ]);
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues.
    echo 'Europa SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
header('Content-Type: application/pdf');
echo $pdf;
```
