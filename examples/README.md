# Examples
---
This folder contains examples how to use Europa API and SDK for PHP.

## Typical usage
---
The typical usage in a project looks like this.

Firstly test connection with the API using `echoApi()` API method.

At the beginning of the selling process use `getQuotes()` API method to get insurance offers from the API and present them to the client to help him/her choose the insurance product he/she is insteresed in.

After the client chooses the insurance product, collect required data on your form and use `makeCalculation()` API method to calculate final insurance price to be presented to the client before purchase.

The insurance form can be generated automatically using Europa SDK for JavaScript - see `insurance_form.md` example.

After the purchase use `issuePolicy()` API method to issue an insurance policy and send it to the client e-mail address.

Please see each example for more detailed description.
