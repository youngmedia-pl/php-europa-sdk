<?php

/**
 * This file is part of the TU Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\HttpClients;

/**
 * Class EuropaCurl
 *
 * Procedural curl as object so that curl can be mocked and the implementation can be tested.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class EuropaCurl
{
    /**
     * @var resource Curl instance.
     */
    protected $curl;

    /**
     * Inits a new curl instance.
     */
    public function init()
    {
        $this->curl = curl_init();
    }

    /**
     * Sets an array of options to a curl resource.
     *
     * @param array $options
     */
    public function setoptArray(array $options)
    {
        curl_setopt_array($this->curl, $options);
    }

    /**
     * Sends a curl request.
     *
     * @return mixed
     */
    public function exec()
    {
        return curl_exec($this->curl);
    }

    /**
     * Returns the curl error number.
     *
     * @return int
     */
    public function errno()
    {
        return curl_errno($this->curl);
    }

    /**
     * Returns the curl error message.
     *
     * @return string
     */
    public function error()
    {
        return curl_error($this->curl);
    }

    /**
     * Closes the resource connection to curl.
     */
    public function close()
    {
        curl_close($this->curl);
    }
}

