<?php

/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

namespace Europa;

use Europa\Exceptions\EuropaSDKException;

/**
 * Class EuropaOperator
 *
 * @package Europa
 */
class EuropaOperator
{
    /**
     * @var string The Europa customer ID.
     */
    protected $customerId;

    /**
     * Instantiates a new Operator entity.
     *
     * @param string $customerId
     *
     * @throws EuropaSDKException
     */
    public function __construct($customerId)
    {
        if (!is_string($customerId)) {
            throw new EuropaSDKException('The "customer_id" must be a string.');
        }

        $this->customerId = $customerId;
    }

    /**
     * Returns the customer ID.
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }
}

