<?php

/**
 * This file is part of the TU Europa SDK for PHP.
 *
 * Copyright Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Europa\ApiObjects;

/**
 * Class ApiOrderStatus
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Europa
 */
class ApiOrderStatus extends ApiObject
{
    /**
     * Returns the status of the order.
     *
     * enum {OK, WARN, ERR}
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->getField('status');
    }
}

